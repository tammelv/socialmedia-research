#! /usr/bin/env python
# -*- encoding: utf-8 -*-
# Author Priit Tamme
# priit at tamme dot lv

import urllib
import urllib2
import json
import math
import MySQLdb

dbCfg = {'host': 'localhost', 'user': 'root', 'passwd': 'pass', 'db':'spin'}
db = MySQLdb.connect(host=dbCfg['host'], user=dbCfg['user'], passwd=dbCfg['passwd'], db=dbCfg['db'], use_unicode=True, charset="utf8" )
c = db.cursor()
c.execute('SET NAMES utf8;')
c.execute('SET CHARACTER SET utf8;')
c.execute('SET character_set_connection=utf8;')


CLIENT_ID = ''
CLIENT_SECRET = ''
url = 'https://api.foursquare.com/v2/venues/explore'


getData = {}
getData['client_id'] = CLIENT_ID
getData['client_secret'] = CLIENT_SECRET
getData['v'] = '20130815' 
getData['near'] = 'Turku'
getData["limit"] = 50
getData["offset"] = 0

get = urllib.urlencode(getData)
response = urllib2.urlopen(url + '?' + get);
myjson = json.loads(response.read())
fsPlaces = []

total = myjson["response"]["totalResults"]
pages = math.ceil(total / getData["limit"])

for i in range(0, int(pages)+1):
    getData["offset"] = i * getData["limit"]
    get = urllib.urlencode(getData)
    response = urllib2.urlopen(url + '?' + get);
    myjson = json.loads(response.read())
    items = myjson["response"]["groups"][0]["items"]
    for item in items:
        place = []
        place.append(item["venue"]["id"])
        place.append(item["venue"]["name"])
        place.append(item["venue"]["location"]["lat"])
        place.append(item["venue"]["location"]["lng"])
        #place.append(item["venue"]["location"]["address"])
        place.append(item["venue"]["verified"])
        place.append(item["venue"]["stats"]["checkinsCount"])
        place.append(item["venue"]["stats"]["usersCount"])
	print place
        fsPlaces.append(tuple(place))
       
c = db.cursor()
c.executemany("""INSERT INTO places (source, fs_id, name, lat, lng,  verified, checkins_count, users_count) VALUES ("fs", %s, %s, %s, %s, %s, %s, %s)""", fsPlaces)
db.commit()
