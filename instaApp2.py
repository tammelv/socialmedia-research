#! /usr/bin/env python
# -*- encoding: utf-8 -*-
# Author Priit Tamme
# priit at tamme dot lv
# instagram

import requests
#import urllib3
import json
import math
import time
import sys
import datetime
import MySQLdb

dbCfg = {'host': 'localhost', 'user': 'root', 'passwd': 'pass', 'db':'spin'}
db = MySQLdb.connect(host=dbCfg['host'], user=dbCfg['user'], passwd=dbCfg['passwd'], db=dbCfg['db'], use_unicode=True, charset="utf8" )
c = db.cursor()
c.execute('SET NAMES utf8;')
c.execute('SET CHARACTER SET utf8;')
c.execute('SET character_set_connection=utf8;')

CLIENT_ID = ''
url = 'https://api.instagram.com/v1/media/search'

getData = {}
getData['client_id'] = CLIENT_ID
# Place #2 - Home
getData['lat'] = '60.45386093' 
getData['lng'] = '22.21973643'
getData["distance"] = '5000'

vMinTimestamp = 1383264000 # Nov 1, 2013
vMaxTimestamp = 1414800000 # Nov 1, 2014
vStep = 15*60 # 1 day in seconds
startTime = int(time.time())

c = db.cursor()
c.execute("""SELECT update_timestamp, period_end FROM stat_extra WHERE src ='igt' and ammount = 20 LIMIT 5""");
results = c.fetchall()

for row in results:
    print "Period: %s - %s " % (row[0], row[1])
    middle = int((row[1] + row[0])/2)
    periods = [(row[0], middle), (middle, row[1])]

    for startTimestamp, endTimestamp in periods:
        getData['min_timestamp'] = startTimestamp
        getData['max_timestamp'] = endTimestamp
        print getData

        response = requests.get(url, params=getData)
        if ('X-Ratelimit-Remaining' not in response.headers):
            print(str(response.status_code))
        else:
            print(str(response.status_code) + ' ' + response.headers['X-Ratelimit-Remaining'])
        while (response.status_code != 200):
            print ('response not 200, sleeping')
            time.sleep(60)
            response = requests.get(url, params=getData)
        responseJson = response.json()
        #print responseJson
        events = []

        for item in responseJson["data"]:
            event = []
            event.append(item["id"])
            event.append(item["created_time"])
            event.append(item["location"]["latitude"])
            event.append(item["location"]["longitude"])
            event.append(item["location"]["latitude"])
            event.append(item["location"]["longitude"])
            event.append(b','.join(tag.encode('utf-8') for tag in item["tags"]))
            if "name" in item["location"].keys():
                event.append(item["location"]["name"])
            else:
                event.append("")
            if "id" in item["location"].keys():
                event.append(item["location"]["id"])
            else:
                event.append(0)
            event.append(item["user"]["id"])
            event.append(item["user"]["username"])
            event.append(item["user"]["full_name"])
            event.append(item["type"])
            event.append(json.dumps(item));
            event.append(item["user"]["profile_picture"].encode('utf-8'))
            event.append(item["images"]["standard_resolution"]["url"].encode('utf-8'))
            events.append(event)
            #print event
        c = db.cursor()
        c.executemany("""REPLACE INTO insta (src_id, created_at, lat, lng, text_lat, text_lng, hashtag, place_name, src_place_id, src_user_id, src_username, src_full_name, mediatype, json, profile_pic, stand_pic ) VALUES (%s, %s, %s,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""", events)
        if (getData['min_timestamp'] == row[0]):
            c.execute("""UPDATE stat_extra SET ammount= %s, period_end=%s, upts=current_timestamp WHERE src='igt' AND update_timestamp=%s """, (len(events), getData["max_timestamp"], getData["min_timestamp"]))
        else:
            c.execute("""REPLACE INTO  stat_extra SET ammount= %s, update_timestamp=%s, period_end=%s, src='igt', upts=current_timestamp """, (len(events), getData["min_timestamp"], getData["max_timestamp"]))
        db.commit()
        print "Salvestas %s" % len(events)

