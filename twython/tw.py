from twython import Twython, TwythonError
#import dateutil.parser
import time
import logging
import MySQLdb
from datetime import datetime
import json

dbCfg = {'host': 'localhost', 'user': 'root', 'passwd': 'pass', 'db':'spin'}
db = MySQLdb.connect(host=dbCfg['host'], user=dbCfg['user'], passwd=dbCfg['passwd'], db=dbCfg['db'], use_unicode=True, charset="utf8" )
c = db.cursor()
c.execute('SET NAMES utf8;')
c.execute('SET CHARACTER SET utf8;')
c.execute('SET character_set_connection=utf8;')
logging.basicConfig(filename="/home/pi/spin/tw.log", level=logging.DEBUG)


APP_KEY = ''
APP_SECRET = ''
OAUTH_TOKEN = ''
OAUTH_TOKEN_SECRET = ''

# Requires Authentication as of Twitter API v1.1
c = db.cursor()
#c.execute("""SELECT max(src_id) FROM statuses where src='tw2' and src_id is not null""");
#result = c.fetchone()
#
#if (result[0] is not None):
#    since_id = result[0]
#else:
#    since_id = 533324822466793473
#
#until_id = 0 #536553720348413952

c.execute("""SELECT min(src_id) FROM statuses where src='tw2' and src_id is not null""");
result = c.fetchone()
if (result[0] is not None):
    until_id = result[0]
else:
    until_id = 0
since_id = 0


last_id = 0 
queryCount = 0;
twitter = Twython(APP_KEY, APP_SECRET, OAUTH_TOKEN, OAUTH_TOKEN_SECRET)

print 'until....'
print since_id

while True:
    queryCount = queryCount + 1
    # Emmergy break
    if (queryCount == 10):
        break
    print queryCount

    #print "unit: %s; since: %s; last: %s " % (until_id, since_id, last_id)
   
    # when max id is smaller from since id will quite workking
    if (until_id > 0 and until_id < since_id):
        break

    try:
	if until_id > 0:
	    print " %s < %s " % (since_id, until_id)
            #search_results = twitter.search(q='', geocode='60.45148,22.26869,5km',  count=100, max_id=until_id)
            search_results = twitter.search(q='', geocode='60.45148,22.26869,5km',  count=50)
	else:
	    print " %s < ... " % (since_id)
            #search_results = twitter.search(q='', geocode='60.45148,22.26869,5km',  count=100, since_id=since_id)
            search_results = twitter.search(q='', geocode='60.45148,22.26869,5km',  count=50)
		
    except TwythonError as e:
        print e
    	logging.info( '-- Error ---')
    print search_results

    if 'errors' in search_results:
	print search_results['errors']

    if search_results['statuses'] is not None:
    	logging.info( len(search_results['statuses']))
    print "staatusi: %s" % (len(search_results['statuses']))
    events = []
    max_id = 0

    for tweet in search_results['statuses']:
        last_id = tweet['id']
	if tweet['id'] > max_id:
	    max_id = tweet['id']

        if tweet['geo'] is not None:
       	    event = []
            created_at = datetime.strptime(tweet['created_at'], '%a %b %d %H:%M:%S +0000 %Y')
       	    event.append(time.strftime('%Y-%m-%d %H:%M:%S', created_at.timetuple()))
            lat = tweet["geo"]["coordinates"][0]
	    lon = tweet["geo"]["coordinates"][1]
       	    event.append(tweet["geo"]["coordinates"][0])
       	    event.append(tweet["geo"]["coordinates"][1])
       	    event.append(tweet["id"])
       	    event.append(tweet["user"]["name"])
       	    event.append('')
       	    event.append(tweet["user"]["id"])
       	    event.append('')

       	    if "name" in tweet["place"].keys():
    	        if "id" in tweet["place"].keys():
    	            event.append(tweet["place"]["id"] + '//' + tweet["place"]["name"])
    	        else:
    	            event.append(tweet["place"]["name"])
    	    else:
                event.append("")

    	    if "id" in tweet["place"].keys():
    	        event.append(tweet["place"]["id"])
    	    else:
    	        event.append(0)

	    event.append( json.dumps(tweet) )
	    if lat > 0 and lon > 0:
	        events.append(event)
	    
	    hashtags = u'';
            print tweet['entities']['hashtags']

	    if "entities" in tweet and len(tweet['entities']['hashtags'])>0:
		for hashtag in tweet['entities']['hashtags']:
		    hashtags = hashtags + ';' + hashtag['text']
	    event.append(hashtags)
    print "last id %s" % (last_id)
    until_id = last_id - 1
    c = db.cursor()

    print "salvestan %s " % ( len(events) )
    c.executemany("""REPLACE INTO events (source, event_timestamp, lat, lng, src_id, src_username, src_full_name, src_user_id, type, place_name, src_place_id, raw, hashtag) VALUES ("tw2", UNIX_TIMESTAMP(%s), %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""", events)
    
    print "Status update last id: %s " % (max_id)
    if last_id >0:
	c.execute("""insert into statuses (update_timestamp, src, src_id, ammount) VALUES (unix_timestamp(), 'tw2', %s, 1)""", ( last_id))
    db.commit()

    if (len(search_results['statuses']) == 0):
        break;
	
